import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TablesComponent } from './components/tables.component';
import { userAdminComponent } from './components/userAdmin.component';
import { driverAdminComponent } from './components/driverAdmin.component';
import { MapComponent } from './components/map.component';
import { settingsComponent } from './components/settings.component';

//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';

import { CurrentBookComponent } from './components/currentBooking.component';
import { PreviousBookComponent } from './components/previousBooking.component';
import { PromoCodeComponent } from './components/promoCode.component';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'pages/login',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'dashboard',
        loadChildren: './dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'components',
        loadChildren: './components/components.module#ComponentsModule'
      },
      {
        path: 'icons',
        loadChildren: './icons/icons.module#IconsModule'
      },
   /*   {
        path: 'forms',
        loadChildren: './forms/forms.module#FormsModule'
      },*/
      {
        path: 'plugins',
        loadChildren: './plugins/plugins.module#PluginsModule'
      },
      {
        path: 'widgets',
        loadChildren: './widgets/widgets.module#WidgetsModule'
      },
      {
        path: 'charts',
        loadChildren: './chartjs/chartjs.module#ChartJSModule'
      },
      {
        path: 'uikits',
        loadChildren: './uikits/uikits.module#UIKitsModule'
      },
      {
        path: 'billings',
        component: TablesComponent,
        data: {
          title: 'Tables'
        }
      },
      {
        path: 'userAdmin',
        component: userAdminComponent,
        data: {
          title: 'User Admin'
        }
      },
      {
        path: 'driverAdmin',
        component: driverAdminComponent,
        data: {
          title: 'Driver Admin'
        }
      },
      {
        path: 'driversonline',
        component: MapComponent,
        data: {
          title: 'map'
        }
      },
      {
        path: 'settings',
        component: settingsComponent,
        data: {
          title: 'settings'
        }
      },
      {
        path: 'currentBook',
        component: CurrentBookComponent,
        data: {
          title: 'currentBook'
        }
      },
      {
        path: 'previousBook',
        component: PreviousBookComponent,
        data: {
          title: 'previousBook'
        }
      },
      {
        path: 'promoCode',
        component: PromoCodeComponent,
        data: {
          title: 'promoCode'
        }
      }
    ]
  },
  {
    path: 'pages',
    component: SimpleLayoutComponent,
    data: {
      title: 'Pages'
    },
    children: [
      {
        path: '',
        loadChildren: './pages/pages.module#PagesModule',
      }
    ]
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
