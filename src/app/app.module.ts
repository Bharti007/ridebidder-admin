import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';
import { AgmCoreModule } from 'angular2-google-maps/core';

import {PopupModule} from 'ng2-opd-popup';

import { AppComponent } from './app.component';
import { DropdownModule } from 'ng2-bootstrap/dropdown';
import { TabsModule } from 'ng2-bootstrap/tabs';
import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';

import { ChartsModule } from 'ng2-charts/ng2-charts';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';

import { TablesComponent } from './components/tables.component';
import { userAdminComponent } from './components/userAdmin.component';
import { driverAdminComponent } from './components/driverAdmin.component';
import { MapComponent } from './components/map.component';
import { settingsComponent } from './components/settings.component';

import { CurrentBookComponent } from './components/currentBooking.component';
import { PreviousBookComponent } from './components/previousBooking.component';
import { PromoCodeComponent } from './components/promoCode.component';

// Routing Module
import { AppRoutingModule } from './app.routing';

//Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';
import { FormsModule } from '@angular/forms';

//firebase
import * as firebase from 'firebase';
  // var config = {
  //   apiKey: "AIzaSyAssD9rndh9pnMtIaA3FJU_JaVfl7bTSdA",
  //   authDomain: "pickmyride-7c5be.firebaseapp.com",
  //   databaseURL: "https://pickmyride-7c5be.firebaseio.com",
  //   projectId: "pickmyride-7c5be",
  //   storageBucket: "pickmyride-7c5be.appspot.com",
  //   messagingSenderId: "735130465689"
  // };

  var config = {
      apiKey: "AIzaSyDv0QzTsHlTfp0nqWK8rPlzXLjhGd2FkcY",
      authDomain: "bidrider-app.firebaseapp.com",
      databaseURL: "https://bidrider-app.firebaseio.com",
      projectId: "bidrider-app",
      storageBucket: "bidrider-app.appspot.com",
      messagingSenderId: "945081768290"
    };
  firebase.initializeApp(config);

@NgModule({
  imports: [FormsModule,
    BrowserModule,
    AppRoutingModule,
    DropdownModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    PopupModule.forRoot(),
    AgmCoreModule.forRoot({
        apiKey: 'AIzaSyBRs8dJH8luo1qJLnK9XdoAFPzB-ZksGN4 '
        }),
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    SimpleLayoutComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    TablesComponent,
    userAdminComponent,
    driverAdminComponent,
    MapComponent,
    settingsComponent,
    CurrentBookComponent,
    PreviousBookComponent,
    PromoCodeComponent
  ],
  providers: [{
    provide: LocationStrategy,
    useClass: HashLocationStrategy
  }],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
