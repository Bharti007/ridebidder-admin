import { Component } from '@angular/core';

@Component({
	moduleId: module.id,
	selector: 'map-cmp',
	templateUrl: 'map.component.html',
	//styleUrls: ['./home.css']
	
})

export class MapComponent {

    lat: number = -33.865143;
    lng: number = 151.209900;
}