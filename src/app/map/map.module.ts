import { NgModule } from '@angular/core';
import { AgmCoreModule } from 'angular2-google-maps/core';
import { MapComponent } from './map.component';

@NgModule({
    imports: [        
        AgmCoreModule.forRoot({
        apiKey: 'AIzaSyBRs8dJH8luo1qJLnK9XdoAFPzB-ZksGN4 '
        }),
    ],
    declarations: [MapComponent],
    exports: [MapComponent]
})

export class MapModule {
       
 }
