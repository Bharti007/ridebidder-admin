import { Component } from '@angular/core';
import * as firebase from 'firebase';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  templateUrl: 'login.component.html'
})
export class LoginComponent {
  errorMsg;
  constructor(  private router: Router) { 
    this.errorMsg="";
  }

  doAdminLogin(email, password){
    if(email==undefined || password == undefined || email == "" || password == ""){
        alert("email or password invalid");
    }else{
     firebase.auth().signInWithEmailAndPassword(email, password).then((authenticatedUser) => {
         console.log("success");
     })
     .catch((error)=>{
        this.errorMsg = "Invalid Email OR Password !!";
         console.log(error);
     });

    }
  }

}
