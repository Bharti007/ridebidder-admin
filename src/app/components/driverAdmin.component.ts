import { Component ,ViewChild } from '@angular/core';
import {Popup} from 'ng2-opd-popup';
//firebase
import * as firebase from 'firebase';

@Component({
  templateUrl: 'driverAdmin.component.html'
})
export class driverAdminComponent {
@ViewChild('popup5') popup5: Popup;
@ViewChild('popup6') popup6: Popup; 
  
  driverData:any;
  value:any;
  userData=[];
  class:any;
  ref:any;

  ratings:any;
  count:any;
  ratingCount:any;
  starRating:any;
    constructor() { 
       this.ref = firebase.database().ref('users');
       this.ref.on ('value',(snapshot:any)=>{
            this.driverData = [];
            if(snapshot.val()){
                this.value = snapshot.val(); 
                console.log(snapshot.val());
                    for (var key in this.value){
                      this.value[key].uid = key;
                              //ratings start
                              let userRef = firebase.database().ref('users/' + this.value[key].uid);
                              userRef.once('value',(snapdata:any)=>{
                                this.ratingCount = 0;
                                if(snapdata.val()){
                                  this.count = 0;
                                  if(snapdata.val().ratings != undefined){
                                    console.log(snapdata.val().ratings);
                                    this.ratings = snapdata.val().ratings;
                                        for (var key in this.ratings){
                                          this.count += 1;
                                          this.ratingCount += this.ratings[key];
                                          this.starRating = (this.ratingCount/this.count).toFixed(1);
                                        //  console.log(this.ratings[key]);
                                        }                   
                                          // console.log("starrating is:" + this.starRating);
                                  }else{
                                    this.starRating= " ";
                                  }
                                }
                              })
                      this.value[key].avgRating = (this.starRating);
                      //ratings end
                      if(this.value[key].driver == true){
                          this.driverData.push(this.value[key]);
                      }


                    }
              console.log(this.driverData);
            }
      })

   /*   let userRef = firebase.database().ref('users/' + 'HH7ktE3Ksrdashu0jQEwHc8t6Or2');
      userRef.once('value',(snapdata:any)=>{
        this.ratingCount = 0;
        if(snapdata.val()){
          this.count = 0;
          if(snapdata.val().ratings != undefined){
            console.log(snapdata.val().ratings);
            this.ratings = snapdata.val().ratings;
                for (var key in this.ratings){
                  this.count += 1;
                  this.ratingCount += this.ratings[key];
                  this.starRating = (this.ratingCount/this.count);
                  console.log(this.ratings[key]);
                  //console.log("starrating is:" + this.starRating);
                }
                console.log("starrating is:" + this.starRating);
          }
        }
      })*/


    }

  //edit user
  currentId:any;
  editDriverForm(i){
    console.log(this.driverData[i]);
    this.userData = this.driverData[i];
    this.currentId = this.driverData[i].uid;

    this.popup5.options = {
                cancleBtnClass: "btn btn-danger", 
                confirmBtnClass: "btn btn-warning btn-mbe-attack",
                color: "#587fcd",
                header: "Update user Details",
                widthProsentage:40,
                animation: "fadeInDown",
                confirmBtnContent: "Update"}
        this.popup5.show(this.popup5.options);
    }

    updateUser(){
        console.log(this.userData);
        this.ref.child(this.currentId).update(this.userData);
        this.popup5.hide();
        this.popup6.hide();
    }


    DriverDetailsForm(i){
    console.log(this.driverData[i]);
    this.userData = this.driverData[i];
    this.currentId = this.driverData[i].uid;

    this.popup6.options = {
                cancleBtnClass: "btn btn-danger", 
                confirmBtnClass: "btn btn-warning btn-mbe-attack",
                color: "#587fcd",
                header: "Update user Details",
                widthProsentage:40,
                animation: "fadeInDown",
                confirmBtnContent: "Update"}
        this.popup6.show(this.popup6.options);
    }

    //change Active status
    changeDriverStatus(index){
      let activeStatus = true;
      let deactiveStatus = false;
          if(this.driverData[index].active == true){
              let statusRef = firebase.database().ref('users/'+ this.driverData[index].uid);
              statusRef.child('active').set(deactiveStatus);
          }else{
              let statusRef = firebase.database().ref('users/'+ this.driverData[index].uid);
              statusRef.child('active').set(activeStatus);
          }
    }

}

