import { Component ,ViewChild } from '@angular/core';
import {Popup} from 'ng2-opd-popup';
//firebase
import * as firebase from 'firebase';

@Component({
  templateUrl: 'userAdmin.component.html'
})
export class userAdminComponent {
//@ViewChild('popup1') popup1: Popup; 
@ViewChild('popup5') popup5: Popup;
@ViewChild('popup6') popup6: Popup; 

  data:any;
  value:any;
  userData=[];
  class:any;
  ref:any;
    constructor() { 
        this.ref = firebase.database().ref('users');
        this.ref.on ('value',(snapshot:any)=>{
              this.data = [];
              if(snapshot.val()){
                  this.value = snapshot.val(); 
                  console.log(snapshot.val());
                      for (var key in this.value){
                        this.value[key].uid = key;
                        if(this.value[key].customer == true){
                          this.data.push(this.value[key]);
                        }
                      }
                console.log(this.data);
              }
        })
    }

  //edit user
  currentId:any;
  editUserForm(i){
    console.log(this.data[i]);
    this.userData = this.data[i];
    this.currentId = this.data[i].uid;

    this.popup5.options = {
                cancleBtnClass: "btn btn-danger", 
                confirmBtnClass: "btn btn-warning btn-mbe-attack",
                color: "#587fcd",
                header: "Update user Details",
                widthProsentage:40,
                animation: "fadeInDown",
                confirmBtnContent: "Update"}
        this.popup5.show(this.popup5.options);
    }

    updateUser(){
        console.log(this.userData);
        this.ref.child(this.currentId).update(this.userData);
        this.popup5.hide();
        this.popup6.hide();
    }

    UserDetailsForm(i){
        console.log(this.data[i]);
        this.userData = this.data[i];
        this.currentId = this.data[i].uid;

        this.popup6.options = {
                    cancleBtnClass: "btn btn-danger", 
                    confirmBtnClass: "btn btn-warning btn-mbe-attack",
                    color: "#587fcd",
                    header: "Update user Details",
                    widthProsentage:10,
                    animation: "fadeInDown",
                    confirmBtnContent: "Update"}
            this.popup6.show(this.popup6.options);
    }

    //change Active status
    changeUserStatus(index){
      let activeStatus = true;
      let deactiveStatus = false;
          if(this.data[index].active == true){
              let statusRef = firebase.database().ref('users/'+ this.data[index].uid);
              statusRef.child('active').set(deactiveStatus);
          }else{
              let statusRef = firebase.database().ref('users/'+ this.data[index].uid);
              statusRef.child('active').set(activeStatus);
          }
    }

}
