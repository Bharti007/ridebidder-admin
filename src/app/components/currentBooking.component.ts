import { Component, ViewChild } from '@angular/core';
import {Popup} from 'ng2-opd-popup';

//firebase
import * as firebase from 'firebase';

@Component({
  templateUrl: 'currentBooking.component.html'
})
export class CurrentBookComponent {
@ViewChild('popup5') popup5: Popup;
@ViewChild('popup1') popup1: Popup;
  driverData:any;
  userData=[];
  class:any;
  ref:any;
currentbookingKey:any;
  //edit user
  currentId:any;

cBooking:any;
value:any;
    constructor() {
      let ref = firebase.database().ref('bookings');
        ref.on ('value',(snapshot:any)=>{
          this.cBooking = [];
          if(snapshot.val()){
            this.value = snapshot.val(); 
            for (var key in this.value){
              this.value[key].bookingKey = key;
              if(this.value[key].pickup != undefined && this.value[key].drop != undefined){
                console.log("pickup and drop location found");
                  if(this.value[key].status != "ENDED" && this.value[key].status != "CANCELLED"){
                    this.cBooking.push(this.value[key]);
                  }
              }

             // console.log(this.data);
            }
            console.log(this.cBooking);

          }
      })

    }

    updateUser(){
        console.log(this.userData);
        //this.ref.child(this.currentId).update(this.userData);
        this.popup5.hide();
        //this.popup6.hide();
    }

    deleteBooking(i){
       let ref = firebase.database().ref('bookings');
        console.log(this.currentbookingKey);
        ref.child(this.currentbookingKey).remove();
        this.popup1.hide();
       // this.popup6.hide();
    }
addPickup:any;
addDrop:any;
    cBookView(i){

   console.log(this.cBooking[i].pickup);
   this.addPickup = this.cBooking[i].pickup.add;
   console.log(this.addPickup);
   this.addDrop = this.cBooking[i].drop.add;
   this.userData = this.cBooking[i];


    this.popup5.options = {
                //cancleBtnClass: "btn btn-danger", 
                //confirmBtnClass: "btn btn-warning btn-mbe-attack",
                color: "#587fcd",
                header: "View Current Booking",
                widthProsentage:40,
                animation: "fadeInDown",
                //confirmBtnContent: "Update"
              }
        this.popup5.show(this.popup5.options);
    }

      deleteBookView(i){

   console.log(this.cBooking[i].pickup);
   this.addPickup = this.cBooking[i].pickup.add;
   console.log(this.addPickup);
   this.addDrop = this.cBooking[i].drop.add;
   this.userData = this.cBooking[i];
this.currentbookingKey= this.cBooking[i].bookingKey ;
    this.popup1.options = {
                cancleBtnClass: "btn btn-danger", 
                confirmBtnClass: "btn btn-primary btn-mbe-attack",
                color: "#587fcd",
                header: "Delete Booking",
                widthProsentage:40,
                animation: "fadeInDown",
                cancleBtnContent: "Cancel"
              }
        this.popup1.show(this.popup1.options);
    }

/*
openActiveBooking(userId){
  console.log(this.data[userId].status);
}
*/


}
