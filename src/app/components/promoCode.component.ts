import { Component, ViewChild } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  templateUrl: 'promoCode.component.html'
})
export class PromoCodeComponent {
public promoName:any;
public promoDesc:any;
public promoValue:any;
public promoType:any;
    coupons = [
       {name: "By Percentage", value:"by_percentage"},
       {name: "By Amount", value:"by_ammount"}
       //{name: "Free Ride", value:"by_ride"}
     ];
    selectedWorkout = null;
ref:any;
id:any;
    constructor(){
        this.ref = firebase.database().ref()
    }


    updateWorkout(d){
        console.log(d.value);
        this.promoType = d.value;

    }
addPromoCode(){
    if(this.promoName == undefined || this.promoName == "" || this.promoDesc == undefined || this.promoDesc == "" || this.promoType == "" ||
    this.promoType == undefined || this.promoValue == undefined || this.promoValue == "" ) { 
        alert("Please Add Promo Details Properly..");
    }else{
        this.ref.once('value',(snapshot:any)=>{
            if(snapshot.val()){
                if(snapshot.val().promoCodes){
                    let codes = snapshot.val().promoCodes;
                    firebase .database().ref('promoCodes/' + codes.length).set({
                            promoName:this.promoName,
                            promoDesc:this.promoDesc,
                            promoValue:this.promoValue,
                            promoType:this.promoType
                    })
                            this.promoName=" ";
                            this.promoDesc=" ";
                            this.promoValue=" ";

                }firebase .database().ref('promoCodes/0').set({
                        promoName:this.promoName,
                        promoDesc:this.promoDesc,
                        promoValue:this.promoValue,
                        promoType:this.promoType
                    })
                            this.promoName=" ";
                            this.promoDesc=" ";
                            this.promoValue=" ";

            }
        })
    }
}


}