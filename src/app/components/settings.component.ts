import { Component } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  templateUrl: 'settings.component.html'
})
export class settingsComponent {
  ref:any;
   personal_hour:any;
   personal_minimum:any;
   pickup_base_km:any;
 //  pickup_hour:any;
   pickup_km:any;
   pickup_minimum:any;
   pickup_minute:any;


  constructor() {
    this.ref = firebase.database().ref("rates");
    this.ref.on('value',(snapshot:any)=>{
      if(snapshot.val()){
        this.personal_hour=snapshot.val().personal_hour;
        this.personal_minimum=snapshot.val().personal_minimum;
        this.pickup_base_km=snapshot.val().pickup_base_km;
      //  this.pickup_hour=snapshot.val().pickup_hour;
        this.pickup_km=snapshot.val().pickup_km;
        this.pickup_minimum=snapshot.val().pickup_minimum;
        this.pickup_minute=snapshot.val().pickup_minute;
      }
    })

   }

  changeSave(){
      this.ref.update({
        personal_hour:this.personal_hour,
        personal_minimum:this.personal_minimum,
        pickup_base_km:this.pickup_base_km,
       // pickup_hour:this.pickup_hour,
        pickup_km:this.pickup_km,
        pickup_minimum:this.pickup_minimum,
        pickup_minute:this.pickup_minute
      }); 
      alert("Data Saved");

  }

}
