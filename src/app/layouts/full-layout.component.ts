import { Component, ViewChild } from '@angular/core';

import * as firebase from 'firebase';

import {Popup} from 'ng2-opd-popup';



@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})

export class FullLayoutComponent /*implements OnInit*/ {
@ViewChild('popup5') popup5: Popup;
ref:any;


  constructor() { 
    this.ref = firebase.database().ref();
  }

  public disabled:boolean = false;
  public status:{isopen:boolean} = {isopen: false};

  public toggled(open:boolean):void {
    console.log('Dropdown is now: ', open);
  }

  public toggleDropdown($event:MouseEvent):void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  doLogout(){
    firebase.auth().signOut();
  }

  changeEmailDialog(){

   // firebase.auth().currentUser.updateEmail('pradip@gmail.com');
   this.popup5.options = {
            cancleBtnClass: "btn btn-danger", 
            confirmBtnClass: "btn btn-warning btn-mbe-attack",
            color: "#587fcd",
            header: "Change Your Email",
            widthProsentage:30,
            animation: "fadeInDown",
            confirmBtnContent: "Change",
            cancleBtnContent: "Close"
          }
    this.popup5.show(this.popup5.options);
  }

newEmail:any;
  updateEmail(){
      if(this.newEmail == undefined || this.newEmail == ""){
          alert("Fields are blank");
      }else{
          firebase.auth().currentUser.updateEmail(this.newEmail).then(()=>{
            this.ref.child('admin').set(this.newEmail);
            alert("Email Changed");
            
          }).catch((err)=>{
             console.log(err);
          });
          this.popup5.hide();
           
      }
  }
el:any;
  leftToggle(){
    this.el = document.getElementById('menuToggle');
    this.el.classList.remove("sidebar-mobile-show");
  }



  }


 // ngOnInit(): void {}

