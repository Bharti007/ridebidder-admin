import { Component } from '@angular/core';
import * as firebase from 'firebase';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'body',
  template: '<router-outlet></router-outlet>'
})
export class AppComponent {

    constructor(private router: Router){
      firebase.auth().onAuthStateChanged((user) => {
            if (user) {
            //let ref = firebase.database().ref('admins/' + user.uid);
              let ref = firebase.database().ref();
              ref.once('value',(snapshot:any)=> {
                  if(snapshot.val()){
                      if(snapshot.val().admin == user.email){
                        this.router.navigate(['/driversonline']);
                      }else{
                        alert("You are not Admin");
                        firebase.auth().signOut();
                        console.log("code works");
                        // this.router.navigate(['']);
                      }
                  }
              })
               // this.router.navigate(['/dashboard']);
            } else {
                this.router.navigate(['']);               
            }
        });
    }

 }
